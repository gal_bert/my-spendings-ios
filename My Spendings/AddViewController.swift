//
//  EditViewController.swift
//  My Spendings
//
//  Created by Gregorius Albert / 2301854486 on 23/11/21.
//

import UIKit

class AddViewController: UIViewController {

    @IBOutlet weak var titleText: UITextField!
    @IBOutlet weak var nominalText: UITextField!
    @IBOutlet weak var categoryControl: UISegmentedControl!
    
    var spendingTitle:String = ""
    var spendingNominal:Int = 0
    var spendingCategory:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func saveBtn(_ sender: Any) {
        if titleText.text == "" {
            present(Helper.pushAlert(title: "Oops!", message: "Spending title can't be empty!"), animated: true, completion: nil)
        }
        else if titleText.text!.count < 5 {
            present(Helper.pushAlert(title: "Oops!", message: "Spending title must be 5 characters or more!"), animated: true, completion: nil)
        }
        else if nominalText.text == "" {
            present(Helper.pushAlert(title: "Oops!", message: "Spending nominal can't be empty!"), animated: true, completion: nil)
        }
        else if !Helper.validateAllNumber(text: nominalText.text!){
            present(Helper.pushAlert(title: "Oops!", message: "Spending nominal must be all numbers!"), animated: true, completion: nil)
        }
        else if categoryControl.selectedSegmentIndex == -1 {
            present(Helper.pushAlert(title: "Oops!", message: "Category must be selected!"), animated: true, completion: nil)
        }
        else {
            performSegue(withIdentifier: "fromAddToHomepage", sender: self)
        }
        
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        spendingTitle = titleText.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        spendingNominal = Int(nominalText.text!)!
        spendingCategory = categoryControl.selectedSegmentIndex
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @IBAction func titleTextOnExit(_ sender: Any) {
        self.resignFirstResponder()
    }
    
}
