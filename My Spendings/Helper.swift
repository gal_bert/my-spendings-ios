//
//  Helper.swift
//  My Spendings
//
//  Created by Gregorius Albert / 2301854486 on 23/11/21.
//

import Foundation
import UIKit

class Helper{
    
    static func pushAlert(title:String, message:String) -> UIAlertController{
        let alert = UIAlertController(
            title: title,
            message: message,
            preferredStyle: .alert
        )
        
        alert.addAction(UIAlertAction(
            title: "OK",
            style: .default,
            handler: nil
        ))
        
        return alert
    }
    
    static func validateAllNumber(text:String) -> Bool {
        var allNumber = true
        for i in text{
            if !i.isNumber{
                allNumber = false
                break
            }
        }
        return allNumber
    }
    
    // Function obtained from Lab Class
    static func formatNumber(num:String) -> String{
        var result:String = ""
        var count:Int = 0
        
        for i in num.reversed(){
            count += 1
            result = "\(i)\(result)"
            if count % 3 == 0 && count != num.count{
                result = ".\(result)"
            }
        }
        return result
    }
    
}
