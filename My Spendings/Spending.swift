//
//  Spending.swift
//  My Spendings
//
//  Created by Gregorius Albert on 23/11/21.
//

import Foundation

class Spending{
    
    var title: String? = nil
    var nominal: Int? = nil
    var category: Int = 0
    
}
