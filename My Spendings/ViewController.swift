//
//  ViewController.swift
//  My Spendings
//
//  Created by Gregorius Albert / 2301854486 on 23/11/21.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var tableViewBottomConstraint: NSLayoutConstraint!
    
    var spendingList: [Spending] = []
    var spendingTitle:String = ""
    var spendingNominal:Int = 0
    var spendingCategory:Int = 0
    var clickedRow:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        countTotal()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        countTotal()
        NotificationCenter.default.addObserver(self, selector: #selector(onResume), name: UIApplication.willEnterForegroundNotification, object: UIApplication.shared)
    }
    
    @objc func onResume() {
        countTotal()
    }
    
    func countTotal() -> Void {
        
        let defaults = UserDefaults.standard
        let state:Bool = defaults.bool(forKey: "showTotal") ? true : false
        
        if state {
            
            tableViewBottomConstraint.constant = 40
            totalLabel.isHidden = false
            
            var total:Int = 0
            if !spendingList.isEmpty{
                for i in 0...spendingList.count-1{
                    total += spendingList[i].nominal!
                }
            }
            else {
                total = 0
            }
            totalLabel.text = "Total: Rp. \(Helper.formatNumber(num: String(total)))"
        }
        else {
            totalLabel.isHidden = true
            tableViewBottomConstraint.constant = 0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return spendingList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let spending = spendingList[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        
        var thumb:String = ""
        
        if spending.category == 0 {
            thumb = "Food"
        }
        else if spending.category == 1 {
            thumb = "Fun"
        }
        else if spending.category == 2 {
            thumb = "Transport"
        }
        else if spending.category == 3 {
            thumb = "Other"
        }
        
        cell?.imageView?.image = UIImage(named: thumb)
        cell?.textLabel?.text = spending.title!
        cell?.detailTextLabel?.text = "Rp. \(Helper.formatNumber(num: String(spending.nominal!)))"
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        clickedRow = indexPath.row
        spendingTitle = spendingList[indexPath.row].title!
        spendingNominal = spendingList[indexPath.row].nominal!
        spendingCategory = spendingList[indexPath.row].category
        performSegue(withIdentifier: "toEditPageSegue", sender: self)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            spendingList.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            countTotal()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toEditPageSegue" {
            let destination = segue.destination as! EditViewController
            destination.spendingTitle = spendingTitle
            destination.spendingNominal = spendingNominal
            destination.spendingCategory = spendingCategory
        }
    }
    
    @IBAction func unwindToHomepage(_ unwindSegue: UIStoryboardSegue) {
        
        if let source = unwindSegue.source as? AddViewController{
            let spending = Spending()
            spending.title = source.spendingTitle
            spending.nominal = source.spendingNominal
            spending.category = source.spendingCategory
            spendingList.insert(spending, at:0)
            tableView.reloadData()
        }
        else if let source = unwindSegue.source as? EditViewController{
            spendingList[clickedRow].title = source.spendingTitle
            spendingList[clickedRow].nominal = source.spendingNominal
            spendingList[clickedRow].category = source.spendingCategory
            tableView.reloadData()
        }
        
    }
    
    
}

